
from bliss.setup_globals import *

load_script("nexus_writer_config.py")

print("")
print("Welcome to your new 'nexus_writer_config' BLISS session !! ")
print("")
print("You can now customize your 'nexus_writer_config' session by changing files:")
print("   * /nexus_writer_config_setup.py ")
print("   * /nexus_writer_config.yml ")
print("   * /scripts/nexus_writer_config.py ")
print("")
