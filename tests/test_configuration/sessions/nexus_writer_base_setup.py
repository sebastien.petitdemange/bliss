
from bliss.setup_globals import *

load_script("nexus_writer_base.py")

print("")
print("Welcome to your new 'nexus_writer_base' BLISS session !! ")
print("")
print("You can now customize your 'nexus_writer_base' session by changing files:")
print("   * /nexus_writer_base_setup.py ")
print("   * /nexus_writer_base.yml ")
print("   * /scripts/nexus_writer_base.py ")
print("")
