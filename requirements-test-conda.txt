# Conda requirement file (python 2.7)
# Test requirements

py
pytest > 4.5.0
pytest-cov
pytest-mock
lima-camera-simulator-tango
lima-tango-server
scipy
black == 18.6b4
umodbus == 1.0.2.1
socat
ser2net
tango_serialline
pytest-rerunfailures
