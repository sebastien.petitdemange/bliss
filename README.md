Bliss
======

[![build status](https://gitlab.esrf.fr/bliss/bliss/badges/master/build.svg)](https://gitlab.esrf.fr/bliss/bliss/pipelines/master/latest)
[![coverage report](https://gitlab.esrf.fr/bliss/bliss/badges/master/coverage.svg)](https://bliss.gitlab-pages.esrf.fr/bliss/master/htmlcov)

The bliss control library.

Latest documentation from master can be found [here](https://bliss.gitlab-pages.esrf.fr/bliss/master)

In short
--------

To update BLISS from source:
```

conda install --file ./requirements-conda.txt

exit and re-enter into conda environment

pip install --no-deps -e .

```
